package groupe;

import java.io.Serializable;

public class Sujet implements Serializable  {
	private int id;
	private String titre;
	private int id_groupe;
	
	
	public Sujet(int id, String titre) {
		this.id = id;
		this.titre = titre;
		this.id_groupe = 0;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void getId_groupe() {
		return id_groupe;
	}

	public void setId_groupe(int id_groupe) {
		this.id_groupe = id_groupe;
	}


	@Override
	public String toString() {
		return "Sujet [id=" + id + ", titre=" + titre + ", groupe_associé_(libre_si_0)=" + id_groupe + "]";
	}
	
	
	
	

}
