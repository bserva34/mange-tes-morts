package fr.umfds.ter2;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import groupe.Groupe;
import groupe.Sujet;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	Groupe g1 = new Groupe(1,"ABAM");
	Sujet s1 = new Sujet(1,"Sport");
	Sujet s2 = new Sujet(2,"Livre");
	Sujet s3 = new Sujet(3,"Art");
	
	@BeforeEach
	public void init() {
	g1.ajouter(s2);
	g1.ajouter(s3);
	g1.ajouter(s1);
	}
	
	@Test 
    public void testcolis1()
    {
    	assertEquals(1,s1.getId());
    }
	
	@Test 
    public void testcolis2()
    {
    	assertEquals(2,s2.getId());
    }
	
	@Test 
    public void testcolis3()
    {
    	assertEquals(3,s3.getId());
    }
}
